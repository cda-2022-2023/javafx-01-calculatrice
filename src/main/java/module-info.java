module org.stcharles.javafxdemo.javafxdemo {
    requires javafx.controls;
    requires javafx.fxml;


    opens org.stcharles.javafxdemo.javafxdemo to javafx.fxml;
    exports org.stcharles.javafxdemo.javafxdemo;
    exports org.stcharles.javafxdemo.javafxdemo.operation;
    opens org.stcharles.javafxdemo.javafxdemo.operation to javafx.fxml;
}