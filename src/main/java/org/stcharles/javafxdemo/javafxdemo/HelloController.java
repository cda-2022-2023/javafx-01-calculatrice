package org.stcharles.javafxdemo.javafxdemo;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.stcharles.javafxdemo.javafxdemo.operation.Operation;

public class HelloController {
    @FXML private TextField operationInputTextField;
    @FXML private Label resultLabel;

    private StringProperty input;
    private DoubleProperty result;

    public HelloController() {
        this.input = new SimpleStringProperty("turlututu");
        this.result = new SimpleDoubleProperty(0);
    }

    @FXML
    private void initialize() {
        // Input binding
        operationInputTextField.textProperty().bindBidirectional(input);

        // Result binding
        var resultString = Bindings.createStringBinding(
                () -> this.result.asString().getValue(),
                this.result
        );
        resultLabel.textProperty().bind(resultString);
    }

    @FXML
    public void onResultButtonClicked(ActionEvent actionEvent) {
        Operation
                .parse(input.getValue())
                .map(Operation::getResult)
                .ifPresent(result::setValue);
    }
}