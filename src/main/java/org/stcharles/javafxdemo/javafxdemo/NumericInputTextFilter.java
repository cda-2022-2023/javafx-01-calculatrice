package org.stcharles.javafxdemo.javafxdemo;

import javafx.scene.control.TextFormatter;

import java.util.function.UnaryOperator;

public class NumericInputTextFilter implements UnaryOperator<TextFormatter.Change> {
    @Override
    public TextFormatter.Change apply(TextFormatter.Change change) {
        var text = change.getText();
        if (text.matches("\\d*")) {
            return change;
        }

        return null;
    }
}
