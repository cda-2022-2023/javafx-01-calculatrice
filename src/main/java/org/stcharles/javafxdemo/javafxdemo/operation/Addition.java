package org.stcharles.javafxdemo.javafxdemo.operation;

final class Addition extends Operation {
    Addition(double firstOperand, double secondOperand) {
        super(firstOperand, secondOperand);
    }

    @Override
    protected double compute(double firstOperand, double secondOperand) {
        return firstOperand + secondOperand;
    }
}
