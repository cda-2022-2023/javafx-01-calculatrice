package org.stcharles.javafxdemo.javafxdemo.operation;

public class Division extends Operation {
    Division(double firstOperand, double secondOperand) {
        super(firstOperand, secondOperand);
    }

    @Override
    protected double compute(double firstOperand, double secondOperand) {
        return firstOperand / secondOperand;
    }
}
