package org.stcharles.javafxdemo.javafxdemo.operation;

import java.util.Optional;
import java.util.regex.Pattern;

public abstract class Operation {
    private final static Pattern pattern = Pattern.compile("(\\d+)([\\+\\-\\*\\/])(\\d+)");
    private final double firstOperand;
    private final double secondOperand;

    protected Operation(double firstOperand, double secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    public double getResult() {
        return compute(firstOperand, secondOperand);
    }

    abstract protected double compute(double firstOperand, double secondOperand);

    public static Optional<Operation> parse(String text) {
        var matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            return Optional.empty();
        }

        var firstOperand = Double.parseDouble(matcher.group(1));
        var operator = matcher.group(2);
        var secondOperand = Double.parseDouble(matcher.group(3));

        return switch (operator) {
            case "+" -> Optional.of(new Addition(firstOperand, secondOperand));
            case "-" -> Optional.of(new Substraction(firstOperand, secondOperand));
            case "*" -> Optional.of(new Multiplication(firstOperand, secondOperand));
            case "/" -> Optional.of(new Division(firstOperand, secondOperand));

            default -> throw new RuntimeException("Operation parsing error");
        };
    }
}
