package org.stcharles.javafxdemo.javafxdemo.operation;

public final class Substraction extends Operation {
    Substraction(double firstOperand, double secondOperand) {
        super(firstOperand, secondOperand);
    }

    @Override
    protected double compute(double firstOperand, double secondOperand) {
        return firstOperand - secondOperand;
    }
}
